export default function ModalImage({ img }) {
  return <img className="products__product-img" src={img} alt="" />;
}
