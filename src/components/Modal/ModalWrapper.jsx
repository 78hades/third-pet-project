export default function ModalWrapper({ onClick, children }) {
  return (
    <div onClick={onClick} className="modal__wrapper">
      {children}
    </div>
  );
}
