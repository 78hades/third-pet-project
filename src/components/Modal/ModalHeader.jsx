export default function ModalHeader({ img, name, children }) {
  return (
    <>
      {" "}
      <h2>{children}</h2>
      <img className="products__product-img" src={img} alt="" />{" "}
      <p className="products__product-name">{name}</p>
    </>
  );
}
