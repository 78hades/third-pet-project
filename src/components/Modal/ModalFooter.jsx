export default function ModalFooter({
  firstText,
  secondaryText,
  firstClick,
  secondaryClick,
}) {
  return (
    <>
      <div className="modal__footer">
        {firstText && (
          <button onClick={() => firstClick()} className="modal__first-btn">
            {firstText}
          </button>
        )}
        {secondaryText && (
          <button
            onClick={() => secondaryClick()}
            className="modal__second-btn"
          >
            {secondaryText}
          </button>
        )}
      </div>
    </>
  );
}
