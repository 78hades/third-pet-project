import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  countBasket: 0,
  basket: [],
  isBasket: false,
  grandTotal:0,
};

const basketSlice = createSlice({
  name: "basket",
  initialState,
  reducers: {
    fillBasket: (state, action) => {
      state.countBasket += 1;
      const newProduct = { ...action.payload, quantity: 1 };
      newProduct.subtotal = parseFloat(newProduct.price.slice(1)).toFixed(2);
      const index = state.basket.findIndex(
        (product) => product.id === action.payload.id
      );
      if (index !== -1) {
        state.basket[index].quantity += 1;
        state.basket[index].subtotal = (
          parseFloat(state.basket[index].subtotal) +
          parseFloat(newProduct.price.slice(1))
        ).toFixed(2);
      } else {
        state.basket.push(newProduct);
      }
      state.grandTotal = (
        parseFloat(state.grandTotal) + parseFloat(newProduct.price.slice(1))
      ).toFixed(2);
    },

    clearBasket: (state, action) => {
      const index = state.basket.findIndex(
        (product) => product.id === action.payload.id
      );
      if (index !== -1) {
        const removedPrice = parseFloat(state.basket[index].price.slice(1));
        state.grandTotal = (
          parseFloat(state.grandTotal) - removedPrice
        ).toFixed(2);
        if (state.basket[index].quantity > 1) {
          state.basket[index].quantity -= 1;
          state.basket[index].subtotal = (
            parseFloat(state.basket[index].subtotal) - removedPrice
          ).toFixed(2);
        } else {
          state.basket.splice(index, 1);
        }
        state.countBasket -= 1;
      }
    },
    isBasketAction: (state, action) => {
      state.isBasket = action.payload;
    },
    lSCountBasket: (state, action) => {
      state.countBasket = action.payload;
    },
    lSbasket: (state, action) => {
      state.basket = action.payload;
    },
    lSGrandTotal: (state, action) => {
      state.grandTotal = action.payload;
    },
    dataReset: (state) => {
      state.countBasket = 0;
      state.basket = [];
      state.grandTotal = 0;
      localStorage.removeItem('grandTotal')
    },
  },
});

export const {
  fillBasket,
  clearBasket,
  isBasketAction,
  lSCountBasket,
  lSbasket,
  lSGrandTotal,
  dataReset,
} = basketSlice.actions;

export default basketSlice.reducer;
