import { useSelector, useDispatch } from "react-redux";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { NavLink } from "react-router-dom";
import { dataReset } from "../../redux/basket.slice/basket.slice";
import checkMark from "./label_verified_social_media_logo_badge_checkmark_icon_210329.svg";
import Cart from "./Cart";
import * as Yup from "yup";
export default function Checkout() {
  const dispatch = useDispatch();
  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required("First name is required"),
    lastName: Yup.string().required("Last name is required"),
    email: Yup.string().email("Invalid email").required("Email is required"),
    year: Yup.number().required("Year is required"),
    address: Yup.string().required("Last address is required"),
    phoneNumber: Yup.string()
      .min(10, "Phone number should be 10 characters")
      .required("Phone number is required"),
  });
  const initialValues = {
    firstName: "",
    lastName: "",
    year: "2024",
    email: "",
    address: "",
    phoneNumber: "",
  };

  const { basket, grandTotal } = useSelector((state) => state.basket);
  const onSubmit = (values) => {
    dispatch(dataReset());
    console.log(`Information about customer: 
First Name: ${values.firstName},
Last Name: ${values.lastName},
Birth year: ${values.year},
Email: ${values.email},
Address: ${values.address},
Phone number: ${values.phoneNumber},
Purchased goods:${basket.map(
      (product) => `
Name:${product.name}, Quantity:${product.quantity}, Color:${product.color}, Price:${product.price}, Subtotal:${product.subtotal};`
    )},
Crand Total:${grandTotal}`);
  };
  return (
    <>
      {" "}
      <p className="checkout__path">
        <NavLink className="link" to="/">
          Home
        </NavLink>{" "}
        &gt;{" "}
        <NavLink className="link" activeclassname="active" to="/checkout">
          Check out
        </NavLink>
      </p>
      <h1 className="checkout__title">Check Out</h1>
      {basket.length !== 0 ? (
        <section className="wrapper__checkout">
          <Formik
            onSubmit={onSubmit}
            initialValues={initialValues}
            validationSchema={validationSchema}
          >
            <Form className="section__checkout checkout">
              <h2 className="checkout__subtitle">Billing details</h2>
              <div className="checkout__first-name-container container">
                {" "}
                <label className="container__label" htmlFor="firstName">
                  First Name*
                </label>{" "}
                <Field
                  className="container__field"
                  type="text"
                  name="firstName"
                >
                  {({ field, form }) => (
                    <input
                      className="container__field"
                      {...field}
                      placeholder="Enter your first name"
                      onChange={(e) => {
                        const name = e.target.value.replace(
                          /[^a-zA-Zа-яА-Я]/g,
                          ""
                        );
                        form.setFieldValue("firstName", name);
                      }}
                    />
                  )}
                </Field>
                <ErrorMessage
                  className="container__error-message"
                  name="firstName"
                  component="div"
                ></ErrorMessage>
              </div>
              <div className="checkout__last-name-container container">
                {" "}
                <label className="container__label" htmlFor="lastName">
                  Last Name*
                </label>{" "}
                <Field className="container__field" type="text" name="lastName">
                  {({ field, form }) => (
                    <input
                      className="container__field"
                      {...field}
                      placeholder="Enter your last name"
                      onChange={(e) => {
                        const name = e.target.value.replace(
                          /[^a-zA-Zа-яА-Я]/g,
                          ""
                        );
                        form.setFieldValue("lastName", name);
                      }}
                    />
                  )}
                </Field>
                <ErrorMessage
                  className="container__error-message"
                  name="lastName"
                  component="div"
                ></ErrorMessage>
              </div>
              <div className="checkout__year-container container">
                {" "}
                <label className="container__label" htmlFor="year">
                  Birth year*
                </label>{" "}
                <Field className="container__field" as="select" name="year">
                  {new Array(100)
                    .fill("")
                    .map((item, index) => new Date().getFullYear() - index)
                    .map((year) => (
                      <option value={year} key={year}>
                        {year}
                      </option>
                    ))}
                </Field>
                <ErrorMessage
                  className="container__error-message"
                  name="year"
                  component="div"
                ></ErrorMessage>
              </div>
              <div className="checkout__address-container container">
                {" "}
                <label className="container__label" htmlFor="address">
                  Address*
                </label>{" "}
                <Field
                  className="container__field"
                  placeholder="Enter your address"
                  type="text"
                  name="address"
                ></Field>
                <ErrorMessage
                  className="container__error-message"
                  name="address"
                  component="div"
                ></ErrorMessage>
              </div>
              <div className="checkout__email-container container">
                {" "}
                <label className="container__label" htmlFor="email">
                  Email*
                </label>{" "}
                <Field
                  className="container__field"
                  placeholder="email@gmail.com"
                  type="text"
                  name="email"
                ></Field>
                <ErrorMessage
                  className="container__error-message"
                  name="email"
                  component="div"
                ></ErrorMessage>
              </div>
              <div className="checkout__number-container container">
                <label className="container__label" htmlFor="phoneNumber">
                  Number*
                </label>

                <Field
                  className="container__field"
                  type="text"
                  name="phoneNumber"
                >
                  {({ field, form }) => (
                    <input
                      className="container__field"
                      {...field}
                      placeholder="(###)###-##-##"
                      onChange={(e) => {
                        const phoneNumber = e.target.value.replace(
                          /[^\d]/g,
                          ""
                        );
                        if (phoneNumber.length <= 10) {
                          form.setFieldValue("phoneNumber", phoneNumber);
                        }
                      }}
                    />
                  )}
                </Field>
                <ErrorMessage
                  className="container__error-message"
                  name="phoneNumber"
                  component="div"
                />
              </div>
              <button className="checkout__submit" type="submit">
                Check out
              </button>
            </Form>
          </Formik>
          <aside className="cart__wrapper">
            <h2 className="cart__title">Order gaming set</h2>
            <table className="cart">
              <tbody>
                {basket.map((product, index) => (
                  <Cart key={index} product={product} />
                ))}
              </tbody>
            </table>
            <p className="cart__grandtotal">
              <span>Total</span> <span>{grandTotal}</span>
            </p>
          </aside>
        </section>
      ) : (
        <div className="checkout__empty">
          <img src={checkMark} alt="" />{" "}
          <p className="checkout__empty-text">
            Thank you for your purchase!<br></br> Your request is being
            processed . . .
          </p>
        </div>
      )}
    </>
  );
}
