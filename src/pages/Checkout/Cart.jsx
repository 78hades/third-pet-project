import { memo } from "react";
function Cart({ product }) {
  return (
    <tr className="cart__product product">
      <td>
        <img className="product__img" src={product.pathImg} alt="" />
      </td>
      <td className="product__about">
        <p>
          {product.name}
          <span className="product__quantity"> × {product.quantity}</span>
        </p>
        <p className="product__color">Color: {product.color}</p>
      </td>
      <td>
        <p className="product__price">{product.price}</p>
      </td>
    </tr>
  );
}

export default memo(Cart);
