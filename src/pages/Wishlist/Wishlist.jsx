import Favoriteproduct from "./Product";
import Modal from "../../components/Modal/Modal";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

function Wishlist() {
  const { wishlist } = useSelector((state) => state.wishlist);
  const FavProduct = wishlist.map((product, index) => (
    <Favoriteproduct key={index} product={product}></Favoriteproduct>
  ));
  if (wishlist.length === 0) {
    return (
      <>
        {" "}
        <p className="wishlist__path">
          <NavLink className="link" to="/">
            Home
          </NavLink>{" "}
          &gt;{" "}
          <NavLink className="link" activeclassname="active" to="/wishlist">
            {" "}
            Wishlist
          </NavLink>
        </p>
        <div className="wishlist__empty">
          <p className="wishlist__empty-message">
            You have not added any product to wishlist : &#40; . . . <br />
            Continue shopping ---&gt;{" "}
            <NavLink className="link-Home" to="/">
              Home
            </NavLink>{" "}
          </p>
        </div>
      </>
    );
  }
  return (
    <>
      <section className="wishlist">
        {" "}
        <p className="wishlist__path">
          <NavLink className="link" to="/">
            Home
          </NavLink>{" "}
          &gt;{" "}
          <NavLink className="link" activeclassname="active" to="/wishlist">
            {" "}
            Wishlist
          </NavLink>
        </p>{" "}
        <h1 className="wishlist__title">Wishlist</h1>
        {FavProduct}
      </section>
      <Modal></Modal>
    </>
  );
}
export default Wishlist;
