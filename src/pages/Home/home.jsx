import ListProducts from "../../components/GoodsSection/ListProducts";
import Banner from "../../components/Banner/Banner";

export default function Home({
  clickCountBasket,
  setCountFavorites,
  setFavorites,
  favorites,
}) {
  return (
    <>
      <Banner></Banner>
      <ListProducts
        setFavorites={setFavorites}
        favorites={favorites}
        clickCountBasket={clickCountBasket}
        setCountFavorites={setCountFavorites}
      ></ListProducts>
    </>
  );
}
